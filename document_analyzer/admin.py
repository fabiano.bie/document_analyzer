from __future__ import unicode_literals

from django.contrib import admin

from .models import Analyzer, Result


@admin.register(Analyzer)
class AnalyzerAdmin(admin.ModelAdmin):
    list_display = ('label', 'slug', 'type', 'parameter')


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    list_display = ('document_version', 'analyzer', 'parameter', 'value')
    list_display_links = ('document_version', 'analyzer',)
