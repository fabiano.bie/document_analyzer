from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from common import menu_object, menu_secondary, menu_setup, menu_tools
from common.apps import MayanAppConfig
from common.settings import settings_db_sync_task_delay
from documents.models import Document, DocumentVersion
from navigation import SourceColumn
from ocr.signals import post_document_version_ocr

from .classes import DocumentAnalyzeResultHelper, DocumentVersionAnalyzeResultHelper
from .handlers import analyze_document
from .links import (
    link_analyzer_list, link_analyzer_create, link_analyzer_delete, link_analyzer_edit, link_result_list,
    link_document_submit, link_analyzer_document_types, link_document_submit_all
)
from .models import Analyzer, Result
from .tasks import task_do_analyze


def document_analyze_submit(self):
    task_do_analyze.apply_async(args=(self.latest_version.pk,))


def document_version_analyze_submit(self):
    task_do_analyze.apply_async(
        kwargs={'document_version_pk': self.pk},
        countdown=settings_db_sync_task_delay.value
    )


class DocumentAnalyzerApp(MayanAppConfig):
    name = 'document_analyzer'
    test = True
    verbose_name = _('Document Analyzer')

    def ready(self):
        super(DocumentAnalyzerApp, self).ready()

        Document.add_to_class(
            'analyzer_value_of', DocumentAnalyzeResultHelper.constructor
        )

        DocumentVersion.add_to_class(
            'analyzer_value_of', DocumentVersionAnalyzeResultHelper.constructor
        )

        Document.add_to_class('submit_for_analyze', document_analyze_submit)
        DocumentVersion.add_to_class(
            'submit_for_analyze', document_version_analyze_submit
        )

        SourceColumn(
            source=Analyzer, label=_('Label'),
            attribute='label'
        )

        SourceColumn(
            source=Analyzer, label=_('Slug'),
            attribute='slug'
        )

        SourceColumn(
            source=Analyzer, label=_('Type'),
            attribute='type'
        )

        SourceColumn(
            source=Analyzer, label=_('Parameter'),
            attribute='parameter'
        )

        SourceColumn(
            source=Result, label=_('Analyzer'),
            attribute='analyzer'
        )

        SourceColumn(
            source=Result, label=_('Parameter'),
            attribute='parameter'
        )

        SourceColumn(
            source=Result, label=_('Value'),
            attribute='value'
        )

        # bind to Action-Menue
        menu_object.bind_links(
            links=(link_document_submit,), sources=(Document,)
        )

        # bind to Analyzerlist
        menu_object.bind_links(
            links=(link_analyzer_delete, link_analyzer_edit, link_analyzer_document_types),
            sources=(Analyzer,)
        )

        # bind to right menu of analyzer list
        menu_secondary.bind_links(
            links=(link_analyzer_list, link_analyzer_create),
            sources=(
                Analyzer, 'document_analyzer:analyzer_create',
                'document_analyzer:analyzer_list'
            )
        )

        menu_tools.bind_links(
            links=(
                link_document_submit_all,  # link_document_type_submit,
            )
        )

        # bind to setup menu
        menu_setup.bind_links(links=(link_analyzer_list,))

        menu_object.bind_links(
            links=(link_result_list,),
            sources=(DocumentVersion,)
        )

        post_document_version_ocr.connect(
            analyze_document, dispatch_uid='analyze_document',
            sender=None
            # sender=task_do_ocr
        )
