#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
import re

logger = logging.getLogger(__name__)


class RegexTool(object):
    def execute(self, document_version, parameter):
        ps = document_version.pages
        result = []
        method, regex = parameter.split(';', 1)

        if method == 'flag':
            reobj = re.compile(regex,re.DOTALL)
        else:
            reobj = re.compile(regex)

        for p in ps.all():
            logger.debug("running regex {} on page {}.".format(regex, p))
            # logger.debug(p.ocr_content.content)

            for m in reobj.finditer(p.ocr_content.content):
                for n, v in m.groupdict().iteritems():
                    v = re.sub(u'[^a-zA-ZáéíóúÁÉÍÓÚâêîôÂÊÎÔãõÃÕçÇ:., ]', '', v).strip()
                    result.append((n, v))
                    logger.debug('regex match:{}={}'.format(n, v))
                    if method == 'first':
                        return result

        return result
