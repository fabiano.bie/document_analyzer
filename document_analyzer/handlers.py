from __future__ import unicode_literals

from .models import Result


def analyze_document(sender, instance, **kwargs):
    """Analyze document version"""
    Result.objects.process_version(document_version=instance)
