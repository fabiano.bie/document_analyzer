from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from documents.permissions import permission_document_view
from navigation import Link

from .permissions import (
    permission_analyzer_view, permission_analyzer_create,
    permission_analyzer_delete, permission_analyzer_edit,
    permission_analyze_document
)


link_document_submit = Link(
    permissions=(permission_analyze_document,), text=_('Submit to analyze'),
    view='document_analyzer:analyzer_submit', args='object.id'
)

link_document_submit_all = Link(
    icon='fa fa-cogs', permissions=(permission_analyze_document,),
    text=_('Analyze all documents'), view='document_analyzer:analyzer_submit_all'
)

link_analyzer_list = Link(
    icon='fa fa-cogs', permissions=(permission_analyzer_view,),
    text=_('Analyzers'), view='document_analyzer:analyzer_list',
)

link_analyzer_create = Link(
    permissions=(permission_analyzer_create,),
    text=_('Create Analyzer'), view='document_analyzer:analyzer_create'
)

link_analyzer_delete = Link(
    permissions=(permission_analyzer_delete,), tags='dangerous',
    text=_('Delete'), view='document_analyzer:analyzer_delete',
    args='resolved_object.id'
)

link_analyzer_edit = Link(
    permissions=(permission_analyzer_edit,), text=_('Edit'),
    view='document_analyzer:analyzer_edit', args='resolved_object.id'
)

link_result_list = Link(
    permissions=(permission_document_view,), text=_('Analyzer result'),
    view='document_analyzer:result_list', args='object.id'
)

link_analyzer_document_types = Link(
    permissions=(permission_analyzer_edit,), text=_('Document types'),
    view='document_analyzer:setup_analyzer_document_types', args='object.pk'
)
