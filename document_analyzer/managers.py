from __future__ import unicode_literals

import logging

from django.db import models
from django.utils.module_loading import import_string
from documents.models import Document
from metadata.models import MetadataType, DocumentMetadata

logger = logging.getLogger(__name__)


class AnalyzerResultManager(models.Manager):

        def process_version(self, document_version):

            self.clean_version(document_version)

            for analyzer in document_version.document.document_type.analyzers.all():
                analyzer_class = import_string(analyzer.type)
                backend = analyzer_class()
                result = backend.execute(document_version=document_version, parameter=analyzer.parameter)
                logger.debug(result)
                for parameter, value in result:
                    self.create(
                        analyzer=analyzer, document_version=document_version, parameter=parameter, value=value
                    )
                    mtdtype = MetadataType.objects.get_by_natural_key(parameter.lower())
                    doc = DocumentMetadata.objects.filter(document=document_version.document, metadata_type=mtdtype)
                    if not doc:
                        DocumentMetadata.objects.create(document=document_version.document, metadata_type=mtdtype,
                                                        value=value)
                    else:
                        for item in doc:
                            item.value = value
                            item.save()


        def clean_version(self, document_version):
            document_version.analyzerresult.all().delete()

        def rebuild_all_anaylzers(self):
            for result in self.all():
                result.delete()

            for document in Document.objects.all():
                self.process_version(document.latest_version)
