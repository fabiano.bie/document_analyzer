# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document_analyzer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analyzer',
            name='document_version',
            field=models.ForeignKey(related_name='analyzers', verbose_name='DocumentType', to='documents.DocumentType'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analyzer',
            name='type',
            field=models.CharField(default=b'Regex', max_length=50, verbose_name='Analyzer', blank=True, choices=[[b'document_analyzer.backends.regex.RegexTool', b'Regex']]),
            preserve_default=True,
        ),
    ]
