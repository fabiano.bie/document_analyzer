from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from smart_settings import Namespace

namespace = Namespace(name='document_analyzer', label=_('DocumentAnalyzer'))

ANALYZER_CHOICES = [
    ('document_analyzer.backends.regex.RegexTool', 'Regex'),
    ('document_analyzer.backends.exiftool.EXIFTool', 'GetExifData'),
]

setting_analyzer_backend = namespace.add_setting(
    global_name='ANALYZER_BACKEND', default='document_analyzer.backends.regex.RegexTool',
    help_text=_(
        'Full path to the backend to be used to extract the Regex data.'
    )
)

setting_analyzer = namespace.add_setting(
    global_name='DOCUMENTS_ANALYZER', default='Regex',
    help_text=_('Default documents analyzer.')
)

setting_analyzer_choices = namespace.add_setting(
    global_name='DOCUMENTS_ANALYZER_CHOICES', default=ANALYZER_CHOICES,
    help_text=_('List of installed document analyzers.')
)
