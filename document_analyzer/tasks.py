from __future__ import unicode_literals

import logging

from django.db import OperationalError

from document_indexing.models import IndexInstanceNode
from documents.models import DocumentVersion
from lock_manager import LockError
from lock_manager.models import Lock
from mayan.celery import app

from .literals import DO_ANALYZER_RETRY_DELAY, LOCK_EXPIRE
from .models import Result

logger = logging.getLogger(__name__)


@app.task(bind=True, default_retry_delay=DO_ANALYZER_RETRY_DELAY, ignore_result=True)
def task_do_analyze(self, document_version_pk):
    lock_id = 'task_do_analyze_doc_version-%d' % document_version_pk
    try:
        logger.debug('trying to acquire lock: %s', lock_id)
        # Acquire lock to avoid doing Analyze on the same document version more than
        # once concurrently
        lock = Lock.objects.acquire_lock(lock_id, LOCK_EXPIRE)
        logger.debug('acquired lock: %s', lock_id)
        document_version = None
        try:
            document_version = DocumentVersion.objects.get(pk=document_version_pk)
            logger.info(
                'Starting document OCR for document version: %s',
                document_version
            )
            Result.objects.process_version(document_version)
            # TextExtractor.process_document_version(document_version)
        except OperationalError as exception:
            logger.warning(
                'Analyzer error for document version: %d; %s. Retrying.',
                document_version_pk, exception
            )
            raise self.retry(exc=exception)
        except Exception as exception:
            logger.error(
                'Analyzer error for document version: %d; %s', document_version_pk,
                exception
            )
        else:
            logger.info(
                'Analyzer complete for document version: %s', document_version
            )
        finally:
            lock.release()
    except LockError:
        logger.debug('unable to obtain lock: %s' % lock_id)


@app.task(bind=True, default_retry_delay=DO_ANALYZER_RETRY_DELAY, ignore_result=True)
def task_do_rebuild_all_analyzers(self):
    if Lock.check_existing(name__startswith='document_indexing_task_update_index_document'):
        # A document index update is happening, wait
        raise self.retry()

    try:
        lock = Lock.acquire_lock(
            'document_indexing_task_do_rebuild_all_indexes'
        )
    except LockError as exception:
        # Another rebuild is happening, retry later
        raise self.retry(exc=exception)
    else:
        try:
            IndexInstanceNode.objects.rebuild_all_indexes()
        finally:
            lock.release()
