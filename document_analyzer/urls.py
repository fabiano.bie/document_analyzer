from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import (
    AnalyzerListView, AnalyzerCreateView, AnalyzerEditView,
    AnalyzerDeleteView, ResultListView, SetupAnalyzerDocumentTypesView,
    DocumentSubmitView, DocumentAllSubmitView
)

urlpatterns = patterns(
    '',
    url(
        r'^analyzer/(?P<pk>\d+)/submit/$', DocumentSubmitView.as_view(),
        name='analyzer_submit'
    ),
    url(
        r'^analyzer/all/submit/$', DocumentAllSubmitView.as_view(),
        name='analyzer_submit_all'
    ),
    url(
        r'^analyzer/list/$', AnalyzerListView.as_view(),
        name='analyzer_list'
    ),
    url(
        r'^analyzer/create/$', AnalyzerCreateView.as_view(),
        name='analyzer_create'
    ),
    url(
        r'^analyzer/(?P<pk>\d+)/edit/$', AnalyzerEditView.as_view(),
        name='analyzer_edit'
    ),
    url(
        r'^analyzer/(?P<pk>\d+)/delete/$', AnalyzerDeleteView.as_view(),
        name='analyzer_delete'
    ),
    url(
        r'^document_version/(?P<pk>\d+)/result/$',
        ResultListView.as_view(),
        name='result_list'
    ),
    url(
        r'^setup/(?P<pk>\d+)/analyzer_document_types/$',
        SetupAnalyzerDocumentTypesView.as_view(),
        name='setup_analyzer_document_types'
    ),
)
