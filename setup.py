#!/usr/bin/env python

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

with open('README.rst') as f:
    readme = f.read()
with open('HISTORY.rst') as f:
    history = f.read()
with open('LICENSE') as f:
    license = f.read()

setup(
    author='Matthias Loeblich',
    author_email='mloeblich@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Programming Language :: Python',
    ],
    description='Document analyzer app for Mayan EDMS.',
    include_package_data=True,
    install_requires=('sh==1.11',),
    license=license,
    long_description=readme + '\n\n' + history,
    name='document_analzer',
    package_data={'': ['LICENSE']},
    package_dir={'document_analzer': 'document_analzer'},
    packages=['document_analzer', 'document_analzer.backends', 'document_analzer.migrations', 'document_analzer.tests'],
    platforms=['any'],
    url='https://gitlab.com/startmat/document_analyzer',
    version='0.1.0',
    zip_safe=False,
)
